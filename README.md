# Quickwork Assignment

This Assignment is a part of the `Intern` Role at Quickwork. Quickwork has Users over 90+ Countries across the world and is becoming a platform of choice for integration and automation. The primary aim for the Assignment included building an API in NodeJS using Express to send E-Mails using the G-Mail REST API.

### Requirements 

Following Requirements were expressed for developing the API: 

- Obtain a Gmail user's credentials using OAuth 2.0. The OAuth 2.0 process should be initiated by an API call to your server. 
- Store the obtained credentials in a file. 
- Have an API endpoint to execute send email using the credentials previously stored. 
- Include appropriate comments in your code on how to use the APIs written by you. 
- Upload the server code to your Github and share the repository link with us. 

There is no need for a visual interface, only the server code is needed.

## Running the Application 

Below are the steps to run the Application: 

- Go to [Google Cloud Console](https://console.cloud.google.com/) and create a New Project. 
- Click on `API's and Services` and follow the `Credentials`.
- Click on `Create Credentials` and then on the OAuth Client ID. 
- Enter Type as `Web Application` and enter the Name of the Application.
- Specify the Authorized Redirect URI as: [https://developers.google.com/oauthplayground](https://developers.google.com/oauthplayground)
- You will receive the Client ID and Client Secret which you can store privately.
- Go to OAuth Playground now: https://developers.google.com/oauthplayground/
- Click on the `Settings` icon and enable the OAuth Credentials by entering the Client ID and Client Secret from the last steps. 
- Search: **https://mail.google.com** in Select & Authorize API and login with your G-Mail Account.
- Click Exchange Authorization Code and get the Refresh Token.

Now that you have everything you need for your Application Development part, let's clone the Repository: 

- Clone the Repo: `git clone https://gitlab.com/HarshCasper/quickwork-assignment.git`
- `cd` into the Repo: `cd quickwork-assignment`
- Create a `.env` file: `touch .env`
- Open the `.env.example` and copy the content from there and paste it into `.env`
- Specify all the Information in `.env` that we got in the previous steps. 
- Install the Packages: `npm install`
- Run the Server: `npm start`

### Packages

- NodeMailer
- Nodemon 
- Google APIs
- DotEnv

### Endpoints 

| Endpoint | Description  |
|---|---|
| `/`  | The Root Endpoint to display a Sample Message  |
| `/sendmail`  | The POST Endpoint to pass the Information in Request Body and send the Mail  |

### Author 

[Harsh Bardhan Mishra](https://www.linkedin.com/in/harshcasper)