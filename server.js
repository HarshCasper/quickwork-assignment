// Importing Necessary Modules 

const express = require('express');
const nodemailer = require('nodemailer'); 
const bodyParser = require('body-parser');
const {
    google
} = require('googleapis');
const OAuth2 = google.auth.OAuth2;

// Importing the Environment Variables
require('dotenv').config(); 

// Initializing the Port Value
const PORT = process.env.PORT || 3000; 

const app = express();
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

// Initializing the Root GET Request
app.get('/', (req, res) => {
    res.send('Root GET Request: Welcome to the Application')
});

// Setting up the OAuth2 Client by parsing Environment Variables
const oauth2Client = new OAuth2(
    process.env.CLIENT_ID, //Client ID
    process.env.CLIENT_SECRET, //Client Secret
    "https://developers.google.com/oauthplayground" //Redirect URL
);

oauth2Client.setCredentials({
    refresh_token: process.env.REFRESH_TOKEN
});

//Getting the Access Token
const accessToken = oauth2Client.getAccessToken(); 

//Configuring a MiddleWare to send E-Mails
const mailTransport = nodemailer.createTransport({
    service: "gmail",
    auth: {
        type: "OAuth2",
        user: process.env.USER_EMAIL,
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
        accessToken: accessToken
    }
});

//Configuring a POST Request to send to the People
app.post('/sendmail', (req, res) => {
    const mailContent = {
        from: process.env.USER_EMAIL, //Email Address of Sender parsed through Environment Variables 
        to: req.body.toAddress, //Email Address of Receiver taken through the Request Body
        subject: req.body.subject, //Subject of the Mail taken through the Request Body
        generateTextFromHTML: true,
        html: req.body.html //Content of the Mail taken through Request Body
    };
    mailTransport.sendMail(mailContent, (err, res) => {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            mailTransport.close();
            console.log("Sent Successfully");
            res.send("Email is received");
        }
    });
    res.send('Process is executed');
});


app.listen(PORT, (req, res) => {
    console.log(`Listening on the Port ${PORT}`);
})
